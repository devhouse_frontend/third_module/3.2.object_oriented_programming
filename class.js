/**
 * ------------ ПРИМЕР ------------- *
 *               ООП                 *
 * --------------------------------- *
 */

/**
 * -------------- ООП -------------- *
 *               Класс               *
 * --------------------------------- *
 */
class Rectangle {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
}

// Чтобы создать объекты класса используется ключевое слово new.

const square = new Rectangle(5, 5);
const rect = new Rectangle(2, 3);

console.log(square.height); // 5
console.log(rect.width); // 3
console.log(rect)// Rectangle { height: 2, width: 3 }

/**
 * -------------- ООП -------------- *
 *              МЕТОДЫ               *
 * ---------- Прототипный ---------- *
 */
class Rectangle2 {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }

    calcArea() {
        return this.height * this.width;
    }
}

const square2 = new Rectangle2(5, 5);
console.log(square2.calcArea()); // 25

/**
 * -------------- ООП -------------- *
 *              МЕТОДЫ               *
 * ---------- Статический ---------- *
 */
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    static distance(a, b) {
        const dx = a.x - b.x;
        const dy = a.y - b.y;

        return Math.hypot(dx, dy);
    }
}

const p1 = new Point(7, 2);
const p2 = new Point(3, 8);
// 7.211102550927979
console.log(Point.distance(p1, p2));

/**
 * -------------- ООП -------------- *
 *            НАСЛЕДОВАНИЕ           *
 * --------------------------------- *
 */
class Animal{
    constructor(name) {
        this.name = name;
    }
    speak() {
        console.log(this.name + ' издает звук');
    }
}
class Dog extends Animal {
    speak() {
        console.log(this.name + ' гавкает');
    }
}
let dog = new Dog('Полкан');
dog.speak(); // Полкан гавкает

/**
 * -------------- ООП -------------- *
 *            НАСЛЕДОВАНИЕ           *
 * --------------------------------- *
 */
class Animal2{
    constructor(name) {
        this.name = name;
    }
    speak() {
        console.log(this.name + ' издает звук');
    }
}
class Dog2 extends Animal2 {
    speak() {
        super.speak();
        console.log(this.name + ' гавкает');
    }
}
let dog2 = new Dog2('Полкан');
dog2.speak();
// Полкан издает звук
// Полкан гавкает